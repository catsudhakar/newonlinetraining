﻿using OnlineTraining.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineTraining.Models
{
    public class CourseViewModel
    {
        public int CourseId { get; set; }
        [Required]
        [Display(Name = "Course Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        public string CourseName { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }
        public System.DateTime UpdatedDate { get; set; }

        public string Description
        {
            get; set;
        }

        // public List<Cours> CoursesList { get; set; }
        public List<CourseListModel> CoursesList { get; set; }
    }

    public class CourseListModel
    {
        public int CourseId { get; set; }

        public string CourseName { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }
        public System.DateTime UpdatedDate { get; set; }

        public string Description
        {
            get; set;
        }


    }

    public class AdminCourseViewModel
    {
        //public int CourseId { get; set; }

        //public string CourseName { get; set; }
        //public bool IsActive { get; set; }
        //public string UpdatedBy { get; set; }
        //public System.DateTime UpdatedDate { get; set; }
        //public string Description {  get; set;  }

        public List<CourseListModel> CourseList { get; set; }

        public int TotalCourses { get; set; }
        public int TotalSections { get; set; }
        public int TotalTopics { get; set; }
        public int TotalInactiveCourses { get; set; }
    }



    public class CourseSectionsTopics
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public string Description { get; set; }
        public List<Section> Sections { get; set; }
        // public List<SectionsTopics> SectionsTopics { get; set; }
        // public SectionsTopics SectionsTopics { get; set; }
        public List<SectionsTopics> LstSectionsTopics { get; set; }
        // public List<TopicViewModel> Topics { get; set; }
    }

    public class SectionsTopics
    {
        public int SectionId { get; set; }
        // public int CourseId { get; set; }
        public string SectionName { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }
        public System.DateTime UpdatedDate { get; set; }

        public List<Topic> topisBySections { get; set; }



    }

    public class TopisBySections
    {
        public int TopicId { get; set; }
        public string TopicName { get; set; }
        public string PdfLink { get; set; }
        public string VideoLink { get; set; }
        public string Duration { get; set; }
    }

    public class SectionViewModel
    {
        public int SectionId { get; set; }
        [Required]
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        [Required]
        [Display(Name = "Section Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        public string SectionName { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }
        public System.DateTime UpdatedDate { get; set; }

        public List<SectionViewModel> SectionList { get; set; }
    }


    public class TopicViewModel
    {
        public int TopicId { get; set; }
        [Required]
        public int SectionId { get; set; }
        public string SectionName { get; set; }

        public int CourseId { get; set; }
        public string CourseName { get; set; }


        [Required]
        [Display(Name = "Topic Name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        public string TopicName { get; set; }

        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public string Description { get; set; }
        // [FileExtensions(Extensions = "txt,doc,docx,pdf", ErrorMessage = "Please upload valid format")]
        public string PdfLink { get; set; }
        public string VideoLink { get; set; }

        public List<TopicViewModel> TopicList { get; set; }
    }
}