﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Net.Configuration;
using System.Configuration;
using System.Net;

namespace OnlineTraining.Models
{
    public class comman
    {
        public void SendMail()
        {
            try
            {
                MailMessage mailMsg = new MailMessage();

                // To
                mailMsg.To.Add(new MailAddress("to@example.com", "To Name"));

                // From
                mailMsg.From = new MailAddress("sudha.polisetti@gmail.com", "From Name");

                // Subject and multipart/alternative Body
                mailMsg.Subject = "subject";
                string text = "text body";
                string html = @"<p>html body</p>";
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

                // Init SmtpClient and send
                SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("username@domain.com", "yourpassword");
                smtpClient.Credentials = credentials;

                smtpClient.Send(mailMsg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            // Plug in your email service here to send an email.
            //try
            //{
            //    using (SmtpClient client = new SmtpClient())
            //    {

            //        client.EnableSsl = true;
            //        client.Credentials = new System.Net.NetworkCredential();
            //        using (MailMessage mailMessage = new MailMessage())
            //        {
            //            mailMessage.From = new MailAddress(Options.User);
            //            mailMessage.To.Add(email);
            //            mailMessage.IsBodyHtml = true;
            //            mailMessage.Subject = subject;
            //            mailMessage.Body = message;
            //            client.Send(mailMessage);
            //        }
            //    }
            //}
            //catch (System.Exception ex)
            //{

            //    throw ex;
            //}

            SmtpSection secObj = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

            using (MailMessage mm = new MailMessage())
            {
                mm.From = new MailAddress(secObj.From); 
                mm.To.Add(email);
                mm.Subject = subject;
                mm.Body = message;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = secObj.Network.Host; 
                smtp.EnableSsl = secObj.Network.EnableSsl; 
                NetworkCredential NetworkCred = new NetworkCredential(secObj.Network.UserName, secObj.Network.Password);
              
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587; //---- SMTP Server port number. This varies from host to host. 
                smtp.Send(mm);
            }
            return Task.FromResult(0);


        }
    }
}


